<?php 
session_start(); 
//Llamamos al archivo de conexion a bbdd
require('includes/conexion.php');
//Llamamos a la conversion de coordenadas
require('includes/phpcoord.php');
//Llamamos al login
require('includes/login.php');

//llamamos a las librerias que hay en /vendor/
require('vendor/autoload.php');

//Le decimos donde van a ir las plantillas de twig
$loader = new Twig_Loader_Filesystem('views/');

//ESTO PARA DESARROLLO
$twig = new Twig_Environment($loader);

//ESTO PARA PRODUCCION
//$twig = new Twig_Environment($loader, array('cache' => 'cache/'));

$twig->addGlobal('session', $_SESSION);

//Vamos a pensar, a ver, a que controlador queremos llamar
//Por defecto, llamamos a blogController.php
if(isset($_GET['c'])){
  $c=$_GET['c'];
}else{
  $c='productosController.php';
}

//Llamo al controlador
require('controllers/'.$c);

//Desconectas
$conexion->close();
?>