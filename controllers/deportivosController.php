<?php  
//Fichero controllers/deportivosController.php


//El controlador, tiene que llamar al modelo
// de datos, y pasar los resultados a la vista
require('models/centroModel.php');
require('models/centrosModel.php');
$centros=new Centros();

if(isset($_GET['id'])){
	//Este es el UNICO CENTRO
	$elcentro=$centros->dimeElemento($_GET['id']);
	echo $twig->render('centro.html.twig', Array('elcentro'=>$elcentro));
}else{
	//Este es el listado
	$loscentros=$centros->dimeElementos();
	echo $twig->render('centros.html.twig', Array('loscentros'=>$loscentros));
}

?>