<?php  
// Fichero models/centrosModel.php
Class Centros{

	public $elementos;

	public function __construct(){
		$this->elementos=[];
	}

	public function dimeElementos(){
		$url='https://datos.madrid.es/portal/site/egob/menuitem.ac61933d6ee3c31cae77ae7784f1a5a0/?vgnextoid=00149033f2201410VgnVCM100000171f5a0aRCRD&format=json&file=0&filename=212808-0-espacio-deporte&mgmtid=2b279e1d29efb410VgnVCM2000000c205a0aRCRD&preview=full';
		$datos=file_get_contents($url);
		$info=json_decode($datos);
		foreach ($info->{'@graph'} as $elemento) {
			$this->elementos[]=new Centro($elemento);
		}
		return $this->elementos;
	}

	public function dimeElemento($id){
		$url='https://datos.madrid.es/portal/site/egob/menuitem.ac61933d6ee3c31cae77ae7784f1a5a0/?vgnextoid=00149033f2201410VgnVCM100000171f5a0aRCRD&format=json&file=0&filename=212808-0-espacio-deporte&mgmtid=2b279e1d29efb410VgnVCM2000000c205a0aRCRD&preview=full';
		$datos=file_get_contents($url);
		$info=json_decode($datos);
		foreach ($info->{'@graph'} as $elemento) {
			if($elemento->id==$id){
				$micentro=new Centro($elemento);
			}
		}
		return $micentro;
	}

}