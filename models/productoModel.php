<?php  
//Fichero models/productoModel.php

class Producto{
	public $id;
	public $nombre;
	public $descripcion;
	public $precio;
	public $unidades;
	public $fecha;
	public $categoria;
	public $url;

	public function __construct($registro){
		$this->id=$registro['idProd'];
		$this->nombre=$registro['nombreProd'];
		$this->descripcion=$registro['descripcionProd'];
		$this->precio=$registro['precioProd'];
		$this->unidades=$registro['unidadesProd'];
		$this->fecha=$registro['fechaAlta'];
		$this->categoria=$registro['nombreCat'];

    	$caracteres = array('Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', ' '=>'-', ':'=>'');
		$nombre=strtr($this->nombre, $caracteres);
		$nombre=strtolower($nombre);

		$this->url='producto-'.$nombre.'-'.$this->id.'.html';
	}
} //Fin de la class Producto
?>